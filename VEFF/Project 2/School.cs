using System;
using System.Collections.Generic;
using StudentClass;

namespace SchoolClass
{
	public class School
	{
		private string m_name;
		private List<Student> m_students = new List<Student>();

		public School(string Name)
		{
			m_name = Name;
		}

		public void AddStudent(Student student)
		{
			m_students.Add(student);
		}

		public override string ToString()
		{
			string tmp =	"School: " + m_name + System.Environment.NewLine +
							"Number of students: " + m_students.Count + System.Environment.NewLine;
			foreach (Student student in m_students)
			{
				tmp += "".PadLeft(4) + student + System.Environment.NewLine;
			}

			return tmp;
		}
	}
}