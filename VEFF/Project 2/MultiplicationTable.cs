using System;

namespace MultiplicationTable
{
	public class program
	{
		public static void Main(string[] args)
		{
			int number = 0;
			try
			{
				number = Convert.ToInt32(args[0]);
			}
			catch (IndexOutOfRangeException)
			{
				Console.WriteLine("Enter a number after the filename");
				return;
			}

			if (number < 1 || number > 20)
			{
				Console.WriteLine("Enter a number in the range 1-20");
				return;
			}

			string html = HtmlGenerator(number);

			Console.Write(html);
		}

		public static string HtmlGenerator(int number)
		{
			string tmp = 	"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\""
							+ System.Environment.NewLine + 
							"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">"
							+ System.Environment.NewLine +
							"<html xmlns=\"http://www.w3.org/1999/xhtml\">"
							+ System.Environment.NewLine +
							"<head>" + System.Environment.NewLine +
							"".PadLeft(4) + "<title>Generated Multiplication Table</title>"
							+ System.Environment.NewLine +
							"".PadLeft(4) + "<meta http-equiv=\"Content-type\" content=\"text/html;charset=UTF-8\" />"
							+ System.Environment.NewLine +
							"</head>" + System.Environment.NewLine +
							"<body>" + System.Environment.NewLine +
							"".PadLeft(4) + "<table>" + System.Environment.NewLine +
							"".PadLeft(8) + "<caption>Multiplication table for number " + number + "</caption>"
							+ System.Environment.NewLine;

			for (int i = 1; i <= 10; ++i)
			{
				tmp +=	"".PadLeft(8) + "<tr>" + System.Environment.NewLine +
						"".PadLeft(12) + "<td>" + i + "</td>" + System.Environment.NewLine +
						"".PadLeft(12) + "<td>" + i * number + "</td>" + System.Environment.NewLine +
						"".PadLeft(8) + "</tr>" + System.Environment.NewLine;
			}

			tmp += 	"".PadLeft(4) + "</table>" + System.Environment.NewLine +
					"</body>" + System.Environment.NewLine +
					"</html>";

			return tmp;
		}
	}
}