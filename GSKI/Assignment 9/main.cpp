#include <iostream>
#include <cmath>
#include "BloomFilter.h"
using namespace std;

int main()
{
	int nOfItems, nOfBits, nOfHashes;
	double probability = 0.00000000001;

	cin >> nOfItems;

	// Formula from http://hur.st/bloomfilter
	nOfBits = ceil((nOfItems * log(probability)) / -0.48);
	nOfHashes = round(log(2.0) * nOfBits / nOfItems);

	BloomFilter filter(nOfBits, nOfHashes);

	for (int i = 0; i < nOfItems; ++i)
	{
		string s;
		cin >> s;

		filter.add(s);
	}

	int c;
	cin >> c;

	for (int i = 0; i < c; ++i)
	{
		string s;
		cin >> s;

		if (filter.contains(s))
		{
			cout << "Possibly bad" << endl;
		}
		else
		{
			cout << "OK" << endl;
		}
	}

	return 0;
}