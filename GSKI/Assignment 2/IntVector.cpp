#include <cmath>
#include "IntVector.h"

using namespace std;

// Constructors

IntVector::IntVector()
{
    array = new int[INITIAL_CAPACITY];
    capacity = INITIAL_CAPACITY;
    count = 0;
}

IntVector::IntVector(int size, int value)
{
    int tmp = 1, sizeCount = size;
    while (sizeCount > INITIAL_CAPACITY) {
        sizeCount -= INITIAL_CAPACITY;
        tmp++;
    }
    array = new int[INITIAL_CAPACITY * tmp];
    for (int i = 0; i < size; ++i)
    {
        array[i] = value;
    }
    capacity = INITIAL_CAPACITY * tmp;
    count = size;
}

IntVector::IntVector(const IntVector& vec)
{
    count = vec.count;
    capacity = vec.capacity;
    array = new int[capacity];

    for(int i = 0; i < count; i++) {
        array[i] = vec.array[i];
    }
}


IntVector::~IntVector()
{
    delete[] array;
}

// Public member functions

void IntVector::push_back(int elem)
{
    if (count + 1 > capacity)
    {
        int* tmp = new int[capacity + INITIAL_CAPACITY];
        for (int i = 0; i < count; ++i)
        {
            tmp[i] = array[i];
        }
        delete[] array;
        array = tmp;
        capacity += INITIAL_CAPACITY;
    }
    array[count] = elem;
    count++;
}

void IntVector::insert(int index, int elem)
{
    if (index > count || index < 0)
    {
        throw IndexOutOfRangeException();
    }

    if (count + 1 > capacity)
    {
        int* tmp = new int[capacity + INITIAL_CAPACITY];
        for (int i = 0; i < count; ++i)
        {
            tmp[i] = array[i];
        }
        delete[] array;
        array = tmp;
        capacity += INITIAL_CAPACITY;
    }
    for (int i = count; i > index; --i)
    {
        array[i] = array[i - 1];
    }
    array[index] = elem;
    count++;
}

int IntVector::at(int elem) const
{
    if (elem >= count || elem < 0)
    {
        throw IndexOutOfRangeException();
    }

    return array[elem];
}

void IntVector::set_value_at(int index, int elem) const
{
    if (index >= count || index < 0)
    {
        throw IndexOutOfRangeException();
    }

    array[index] = elem;
}

int IntVector::size() const
{
    // TODO: Implement
    return count;
}

bool IntVector::empty() const
{
    if (count < 1)
    {
        return true;
    }
    return false;
}

void IntVector::remove_at(int index)
{
    if (index >= count || index < 0)
    {
        throw IndexOutOfRangeException();
    }
    for (int i = index; i < count - 1; ++i)
    {
        array[i] = array[i + 1];
    }
    array[count - 1] = 0;
    count--;
}

int IntVector::pop_back()
{
    if (count < 1)
    {
        throw EmptyException();
    }
    count--;
    return array[count];
}

void IntVector::clear()
{
    delete[] array;
    array = new int[INITIAL_CAPACITY];
    capacity = INITIAL_CAPACITY;
    count = 0;
}

// Overloaded operators

void IntVector::operator=(const IntVector& vec)
{
    if(capacity < vec.capacity) {
        delete [] array;
        array = new int[vec.capacity];
    }

    capacity = vec.capacity;
    count = vec.count;

    for(int i = 0; i != count; i++) {
        array[i] = vec.array[i];
    }
}

int& IntVector::operator[] (int index) const
{
    if (index >= count || index < 0)
    {
        throw IndexOutOfRangeException();
    }
    return array[index];
}

ostream& operator<< (ostream& out, const IntVector& rhs)
{
    for(int i = 0; i < rhs.size(); i++) {
		if(i > 0) {
			out << " ";
		}
        out << rhs[i];
    }
    return out;
}