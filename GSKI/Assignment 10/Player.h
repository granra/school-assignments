#ifndef PLAYER_H
#define PLAYER_H

class Player
{
public:
	Player();

	virtual int getGuess() = 0;

	void setWins(int n);

	int getWins();

private:
	int wins;
};

Player::Player()
{
	this->wins = 0;
}

void Player::setWins(int n)
{
	this->wins = n;
}

int Player::getWins()
{
	return this->wins;
}

#endif