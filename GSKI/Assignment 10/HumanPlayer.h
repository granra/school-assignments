#ifndef HUMANPLAYER_H
#define HUMANPLAYER_H

#include <iostream>
#include "Player.h"

class HumanPlayer : public Player
{
public:
	virtual int getGuess();
};

int HumanPlayer::getGuess()
{
	std::cout << "Enter your guess [0 - 99]: ";
	int n;
	std::cin >> n;
	return n;
}

#endif