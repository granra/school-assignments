#include <iostream>
#include <cstdlib>
#include <ctime>
#include "HumanPlayer.h"
#include "ComputerPlayer.h"

using namespace std;

bool checkForWin(int guess, int answer, int guessNumber) {
    cout << "Guess no. " << guessNumber << ", you guessed " << guess << ". ";
    if (answer == guess) {
        cout << "You're right! You win!" << endl;
        return true;
    }
    else if (answer < guess) {
        cout << "Your guess is too high." << endl;
    }
    else {
        cout << "Your guess is too low." << endl;
    }
    return false;
}

void play(Player& player1, Player& player2) {
    int answer = 0, guess = 0, guessNumber = 0;
    answer = rand() % 100;
    bool win = false;

    while (!win) {
        cout << "Player 1's turn to guess." << endl;
        guess = player1.getGuess();
        guessNumber++;
        win = checkForWin(guess, answer, guessNumber);
        if (win) {
            player1.setWins(player1.getWins() + 1);
            return;
        }
        cout << "Player 2's turn to guess." << endl;
        guess = player2.getGuess();
        guessNumber++;
        win = checkForWin(guess, answer, guessNumber);
        if (win) {
            player2.setWins(player2.getWins() + 1);
        }
    }
}

void printWelcome()
{
	cout << "Welcome to Grand Theft Guessing!" << endl;
}

void printMenu()
{
	cout << "****************************" << endl
	<< "Select the game type (1-3) from the menu, any other number to quit" << endl
	<< "1. Human vs. Human" << endl
	<< "2. Human vs. Computer" << endl
	<< "3. Computer vs. Computer" << endl
	<< "****************************" << endl;
}

int getSeed()
{
	cout << "Seed: ";
	int seed;
	cin >> seed;
	return seed;
}

int main()
{
    Player *humanPlayer1, *humanPlayer2, *computerPlayer1, *computerPlayer2;
    int n;
    bool running = 1;

	printWelcome();
    srand(getSeed());

    humanPlayer1 = new HumanPlayer();
    humanPlayer2 = new HumanPlayer();
    computerPlayer1 = new ComputerPlayer();
    computerPlayer2 = new ComputerPlayer();

    while (running)
    {
        printMenu();
        cin >> n;
        switch (n)
        {
            case 1:
                play (*humanPlayer1, *humanPlayer2);
                break;
            case 2:
                play (*humanPlayer1, *computerPlayer1);
                break;
            case 3:
                play (*computerPlayer1, *computerPlayer2);
                break;
            default:
                running = 0;
        }
    }
    cout << "********** Results **********" << endl;
    cout << "Human player 1 won " << humanPlayer1->getWins() << " times." << endl;
    cout << "Human player 2 won " << humanPlayer2->getWins() << " times." << endl;
    cout << "Computer player 1 won " << computerPlayer1->getWins() << " times." << endl;
    cout << "Computer player 2 won " << computerPlayer2->getWins() << " times." << endl;

    delete humanPlayer1;
    delete humanPlayer2;
    delete computerPlayer1;
    delete computerPlayer2;

	return 0;
}
