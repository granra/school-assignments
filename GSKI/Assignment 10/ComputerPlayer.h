#ifndef COMPUTERPLAYER_H
#define COMPUTERPLAYER_H

#include "Player.h"

class ComputerPlayer : public Player
{
public:
	virtual int getGuess();
};

int ComputerPlayer::getGuess()
{
	return rand() % 100;
}

#endif