#include <iostream>
#include "Organism.h"
#include "World.h"

using namespace std;

// Create an organism at the given coordinates in the given world.
Organism::Organism(World* aWorld, int xcoord, int ycoord) {
    world = aWorld;
    x = xcoord;
    y = ycoord;
    breedTicks = 0;
    moved = false;
    world->setAt(x, y, this);
}

// flags the organism as moved or not
void Organism::setMoved(bool hasMoved) {
    moved = hasMoved;
}

// has the organism moved or not?
bool Organism::hasMoved() const {
    return moved;
}

// moves the organism from coordinates (x,y) to (xNew,yNew)
void Organism::movesTo(int xNew, int yNew) {
    world->setAt(x, y, NULL);
    x = xNew;
    y = yNew;
    world->setAt(x, y, this);
    setMoved(true);
}

// Breeds an organism at an adjacent cell. This method calls the
// generateOffspring() method.
void Organism::breedAtAdjacentCell()  {
    if (y + 1 < WORLDSIZE && world->getAt(x, y + 1) == NULL)
    {
        generateOffspring(x, y + 1);       
    }
    else if (y - 1 >= 0 && world->getAt(x, y - 1) == NULL)
    {
        generateOffspring(x, y - 1);
    }
    else if (x - 1 >= 0 && world->getAt(x - 1, y) == NULL)
    {
        generateOffspring(x - 1, y);
    }
    else if (x + 1 < WORLDSIZE && world->getAt(x + 1, y) == NULL)
    {
        generateOffspring(x + 1, y);
    }
}

// Returns true if organism is dead, false otherwise.
bool Organism::isDead() const {
    return false;
}
