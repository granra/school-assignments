#ifndef ANT_H
#define ANT_H

#include "Organism.h"

class World;

class Ant : public Organism
{
public:
	Ant(World* aWorld, int xcoord, int ycoord) : Organism(aWorld, xcoord, ycoord) {}

    // In the given world moves this organism.
    virtual void move();

    // Makes this organism breed.
    virtual void breed();

    // Returns the type of this organism.
    virtual OrganismType getType() const;

    // The character representation of this organism.
    virtual char representation() const;

    // The size of this organism.
    virtual int size() const;

private:
    // Generates an offspring at the given position.
    virtual void generateOffspring(int whereX, int whereY);
};

#endif