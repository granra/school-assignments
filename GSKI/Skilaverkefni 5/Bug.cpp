#include <iostream>
#include "Bug.h"

using namespace std;

// In the given world moves this organism.
void Bug::move()
{
	if (y + 1 < WORLDSIZE && world->getAt(x, y + 1) != NULL && world->getAt(x, y + 1)->getType() == ANT)
    {
    	bugMovesTo(x, y + 1);     
    }
    else if (y - 1 >= 0 && world->getAt(x, y - 1) != NULL && world->getAt(x, y - 1)->getType() == ANT)
    {
    	bugMovesTo(x, y - 1);  
    }
    else if (x - 1 >= 0 && world->getAt(x - 1, y) != NULL && world->getAt(x - 1, y)->getType() == ANT)

    {
    	bugMovesTo(x - 1, y);
    }
    else if (x + 1 < WORLDSIZE && world->getAt(x + 1, y) != NULL && world->getAt(x + 1, y)->getType() == ANT)
    {
    	bugMovesTo(x + 1, y); 
    }
    else
    {
    	ticksSinceEaten++;
		switch (world->randomMove())
		{
			case UP: // Up
					if (y + 1 < WORLDSIZE)
					{
						bugMovesTo(x, y + 1);
					}
				break;
			case DOWN: // Down
					if (y - 1 >= 0)
					{
						bugMovesTo(x, y - 1);
					}
				break;
			case LEFT: // Left
					if (x - 1 >= 0)
					{
						bugMovesTo(x - 1, y);
					}
				break;
			case RIGHT: // Right
					if (x + 1 < WORLDSIZE)
					{
						bugMovesTo(x + 1, y);
					}
				break;
		}
		if (ticksSinceEaten >= STARVE_BUGS)
		{
			setDead();
			return;
		}
    }
	breedTicks++;
}

// Makes this organism breed.
void Bug::breed()
{
	if (breedTicks >= BREED_BUGS)
	{
		breedAtAdjacentCell();
	}
}

// Returns the type of this organism.
OrganismType Bug::getType() const
{
	return BUG;
}

// The character representation of this organism.
char Bug::representation() const
{
	return 'X';
}

// The size of this organism.
int Bug::size() const
{
	return 30;
}

// Generates an offspring at the given position.
void Bug::generateOffspring(int whereX, int whereY)
{
	world->setAt(whereX, whereY, new Bug(world, whereX, whereY));
	breedTicks = 0;
}

void Bug::bugMovesTo(int x, int y)
{
	Organism* organism = world->getAt(x, y);

	if (organism != NULL)
	{
		if (organism->getType() == BUG)
		{
			return;
		}
		else if (organism->getType() == ANT)
		{
			delete organism;
			ticksSinceEaten = 0;
		}
	}

	movesTo(x, y);
}

void Bug::setDead()
{
	dead = true;
}

bool Bug::isDead() const
{
	return dead;
}