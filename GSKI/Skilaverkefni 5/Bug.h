#ifndef BUG_H
#define BUG_H

#include "Organism.h"

class World;

class Bug : public Organism
{
public:
	Bug(World* aWorld, int xcoord, int ycoord) : Organism(aWorld, xcoord, ycoord)
    {
        dead = false;
        ticksSinceEaten = 0;
    }

    // In the given world moves this organism.
    virtual void move();

    // Makes this organism breed.
    virtual void breed();

    // Returns the type of this organism.
    virtual OrganismType getType() const;

    // The character representation of this organism.
    virtual char representation() const;

    // The size of this organism.
    virtual int size() const;

    void setDead();

    // Returns true if organism is dead, false otherwise.
    virtual bool isDead() const;

private:
    // Generates an offspring at the given position.
    virtual void generateOffspring(int whereX, int whereY);

    void bugMovesTo(int x, int y);

    bool dead;

    int ticksSinceEaten;
};

#endif