#include <iostream>
#include "Ant.h"

using namespace std;

// In the given world moves this organism.
void Ant::move()
{
	switch (world->randomMove())
	{
		case UP: // Up
			if (y + 1 < WORLDSIZE && world->getAt(x, y + 1) == NULL)
			{
				movesTo(x, y + 1);
			}
			break;
		case DOWN: // Down
			if (y - 1 >= 0 && world->getAt(x, y - 1) == NULL)
			{
				movesTo(x, y - 1);
			}
			break;
		case LEFT: // Left
			if (x - 1 >= 0 && world->getAt(x - 1, y) == NULL)
			{
				movesTo(x - 1, y);
			}
			break;
		case RIGHT: // Right
			if (x + 1 < WORLDSIZE && world->getAt(x + 1, y) == NULL)
			{
				movesTo(x + 1, y);
			}
			break;
	}
	breedTicks++;
}

// Makes this organism breed.
void Ant::breed()
{
	if (breedTicks >= BREED_ANTS)
	{
		breedAtAdjacentCell();
	}
}

// Returns the type of this organism.
OrganismType Ant::getType() const
{
	return ANT;
}

// The character representation of this organism.
char Ant::representation() const
{
	return 'o';
}

// The size of this organism.
int Ant::size() const
{
	return 10;
}

// Generates an offspring at the given position.
void Ant::generateOffspring(int whereX, int whereY)
{
	world->setAt(whereX, whereY, new Ant(world, whereX, whereY));
	breedTicks = 0;
}