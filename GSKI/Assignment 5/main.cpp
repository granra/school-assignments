#include <iostream>
#include "IntQueue.h"
using namespace std;

int main()
{
	IntQueue queue;

	queue.enqueue(1);
	queue.enqueue(2);
	queue.enqueue(3);

	cout << queue << endl;
	cout << queue.front() << endl;

	queue.dequeue();
	
	cout << queue.front() << endl;
	queue.dequeue();
	queue.dequeue();

	try {
		queue.dequeue();
	}
	catch (EmptyQueueException)
	{
		cout << "queue is empty" << endl;
	}

	cout << queue << endl;

	return 0;
}