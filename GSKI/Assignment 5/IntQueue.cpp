#include <iostream>
#include "IntQueue.h"
using namespace std;

// Initializes an empty queue.
IntQueue::IntQueue()
{
	head = NULL;
	tail = NULL;
}

// Destructor
IntQueue::~IntQueue()
{
	for (NodePtr curr = head, tmp; curr != NULL; curr = tmp)
	{
		tmp = curr->link;
		delete curr;
	}
}

// Returns true if and only if the queue is empty
bool IntQueue::empty() const
{
	if (head == NULL && tail == NULL)
	{
		return 1;
	}

	return 0;
}

// Inserts 'item' at the back of the queue.
void IntQueue::enqueue(int item)
{
	if (isEmpty()) // If the list is empty
	{
		head = new IntNode(item, NULL); // Create a new node which is now the only node
		tail = head;
		return;
	}

	tail->link = new IntNode(item, NULL);
	tail = tail->link;
}

// Removes and returns the front of the queue (i.e. the element that
// was enqueued earliest).
// If the queue is empty, EmptyQueueException is thrown.
int IntQueue::dequeue()
{
	if (isEmpty())
	{
		throw EmptyQueueException();
	}

	int tmp = head->data; // Store the data I want to return
	NodePtr nodeTmp = head;	// Store the location of the node I'm about to delete
	head = head->link;	// Move head to the next node
	delete nodeTmp;

	if (head == NULL) // If the recently deleted was the only one
	{
		tail = NULL;
	}

	return tmp;
}

// Returns the front of the queue (i.e. the element that
// was enqueued earliest).
// If the queue is empty, EmptyQueueException is thrown.
int IntQueue::front() const
{
	if (head == NULL && tail == NULL)
	{
		throw EmptyQueueException();
	}

	return head->data;
}

// Prints the elements of the queue to the stream 'outs', from front to
// back, separated by a single space.
ostream& operator<< (ostream& outs, const IntQueue& queue)
{
	for (NodePtr curr = queue.head; curr != NULL; curr = curr->link)
	{
		outs << curr->data << " ";
	}

	return outs;
}

bool IntQueue::isEmpty()
{
	return head == NULL && tail == NULL;
}