#include "Vector.h"

int main(int argc, char const *argv[])
{
	Vector<int> vec;

	vec.push_back(1);

	cout << vec << endl;

	vec.push_back(2);

	cout << vec << endl;

	return 0;
}