#include <iostream>
#include "CharStack.h"
using namespace std;

bool safe_to_publish(string book);

int main()
{
	string book;

	getline(cin, book);

	if (safe_to_publish(book))
	{
		cout << "Publish!" << endl;
	}
	else
	{
		cout << "Fix brackets" << endl;
	}

	return 0;
}

bool safe_to_publish(string book)
{
	CharStack stack; // Create a stack

	for (unsigned int i = 0; i < book.length(); ++i) // Iterate through the string
	{
		switch (book[i])
		{
			case '(':
				//cout << "pushing..." << endl;
				stack.push(book[i]); // Push an opening bracket to the stack
				break;
			case ')':
				try {
					stack.pop(); // 
				}
				catch (EmptyStackException) {
					//cout << "Exception was cought" << endl;
					return 0; 	// If the stack is already empty
				}				// the brackets are wrong
				break;
		}
	}

	//cout << stack << endl;

	if (stack.empty())
	{
		return 1;
	}

	return 0;
}