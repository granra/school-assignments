#include <iostream>
#include "CharStack.h"
using namespace std;

bool safe_to_publish(string book);

bool checkBracket(CharStack& stack, char bracket);

int main()
{
	string book;

	getline(cin, book);

	if (safe_to_publish(book))
	{
		cout << "Publish!" << endl;
	}
	else
	{
		cout << "Fix brackets" << endl;
	}

	return 0;
}

bool safe_to_publish(string book)
{
	CharStack stack; // Create a stack

	for (unsigned int i = 0; i < book.length(); ++i) // Iterate through the string
	{
		switch (book[i])
		{
			case '(':
				//cout << "pushing..." << endl;
				stack.push(book[i]); // Push an opening bracket to the stack
				break;
			case ')':
				if (!checkBracket(stack, '('))
				{
					return 0;
				}
				break;
			case '{':
				stack.push(book[i]);
				break;
			case '}':
				if (!checkBracket(stack, '{'))
				{
					return 0;
				}
				break;
			case '[':
				stack.push(book[i]);
				break;
			case ']':
				if (!checkBracket(stack, '['))
				{
					return 0;
				}
				break;
		}
	}

	//cout << stack << endl;

	if (stack.empty())
	{
		return 1;
	}

	return 0;
}

bool checkBracket(CharStack& stack, char bracket)
{
	char top;
	try
	{
		top = stack.top();
	}
	catch (EmptyStackException)
	{
		return 0;
	}

	if (top == bracket)
	{
		try
		{
			stack.pop();
		}
		catch (EmptyStackException)
		{
			return 0;
		}
	}
	else {
		return 0;
	}

	return 1;
}