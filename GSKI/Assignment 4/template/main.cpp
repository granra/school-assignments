#include <iostream>
#include "CharStack.h"
using namespace std;

bool safe_to_publish(string book);

bool checkBracket(CharStack& stack, char letter);

int main()
{
	string book;

	getline(cin, book);

	if (safe_to_publish(book))
	{
		cout << "Publish!" << endl;
	}
	else
	{
		cout << "Fix brackets" << endl;
	}

	return 0;
}

bool safe_to_publish(string book)
{
	CharStack stack; // Create a stack

	for (unsigned int i = 0; i < book.length(); ++i) // Iterate through the string
	{
		if (book[i] > 64 && book[i] < 91)
		{
			stack.push(book[i]);
		}
		else if (book[i] > 96 && book[i] < 123)
		{
			if (!checkBracket(stack, book[i]))
			{
				return 0;
			}
		}
	}

	//cout << stack << endl;

	if (stack.empty())
	{
		return 1;
	}

	return 0;
}

bool checkBracket(CharStack& stack, char letter)
{
	char top;
	try
	{
		top = stack.top();
	}
	catch (EmptyStackException)
	{
		return 0;
	}

	if (top == (letter - 32))
	{
		try
		{
			stack.pop();
		}
		catch (EmptyStackException)
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}

	return 1;
}