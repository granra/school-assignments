#include <iostream>
#include "linkList.h"

using namespace std;

linkList::linkList() {
	head = NULL;
	tail = NULL;
}

linkList::~linkList() {
	clear();
}

void linkList::push_front(int elem) {
	if (head == NULL) // List is empty
	{
		head = new node;
		tail = head;
		head->data = elem;
		head->next = NULL;
		head->prev = NULL;
		return;
	}

	node* tmp = head;
	head = new node;
	tmp->prev = head;
	head->data = elem;
	head->next = tmp;
	head->prev = NULL;
}

void linkList::push_back(int elem) {
	if (head == NULL) // List is empty
	{
		head = new node;
		tail = head;
		head->data = elem;
		head->next = NULL;
		head->prev = NULL;
		return;
	}

	node* tmp = tail;
	tail = new node;
	tmp->next = tail;
	tail->data = elem;
	tail->next = NULL;
	tail->prev = tmp;
}

void linkList::print_list() {
	if (head == NULL) { // List is empty
		cout << "List is empty" << endl;
		return;
	}

	for (node* curr = head; curr != NULL; curr = curr->next)
	{
		cout << curr->data << " ";
	}
	cout << endl;
}

void linkList::print_backwards() {
	if (tail == NULL) { // List is empty
		cout << "List is empty" << endl;
		return;
	}

	for (node* curr = tail; curr != NULL; curr = curr->prev)
	{
		cout << curr->data << " ";
	}
	cout << endl;
}

void linkList::clear() {
	node* tmp;
	for (node* curr = head; curr != NULL; curr = tmp)
	{
		tmp = curr->next;
		delete curr;
	}
	head = NULL;
	tail = NULL;
}