#include <iostream>
#include "linkList.h"

using namespace std;

int main()
{
	linkList list;
	list.print_list();
	
	for (int i = 0; i < 10; ++i)
	{
		list.push_back(i);
	}

	for (int i = -1; i > -10; --i)
	{
		list.push_front(i);
	}

	list.print_list();

	list.print_backwards();

	list.clear();

	list.print_list();

	list.push_front(1);

	list.print_list();

	return 0;
}

