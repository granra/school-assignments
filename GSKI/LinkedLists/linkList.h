#ifndef LINKLIST_H
#define LINKLIST_H

struct node {
	int data;
	node* next;
	node* prev;
};

class linkList {
private:
	node* head;
	node* tail;
public:
	linkList();
	~linkList();
	void push_front(int elem);
	void push_back(int elem);
	void print_list();
	void print_backwards();
	void clear();
};

#endif