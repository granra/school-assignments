#include <iostream>
#include <vector>
#include "ContactManager.h"
using namespace std;

int main()
{
	ContactManager cm;
	Contact contact1;
	contact1.name = "Favian Hennman";
	contact1.phone = "528-1111";
	
	Contact contact2;
	contact2.name = "Favian Hoeger";
	contact2.phone = "545-1116";

	Contact contact3;
	contact3.name = "Favian Jonsson";
	contact3.phone = "533-1109";

	Contact contact4;
	contact4.name = "Z";
	contact4.phone = "849-4668";


	cm.add(contact3);
	cm.add(contact2);
	cm.add(contact4);
	cm.add(contact1);

	vector<Contact> v = cm.get_contacts_by_name_prefix("Favian");

	for (unsigned int i = 0; i < v.size(); ++i)
	{
		cout << v[i] << endl;
	}

	/*try
	{
		cout << cm.get_contact_by_name("test") << endl;	
	}
	catch (ContactMissingException)
	{
		cout << "That contact does not exist" << endl;
	}
	
	try
	{
		cout << cm.get_contact_by_phone("1234567") << endl;
	}
	catch (ContactMissingException)
	{
		cout << "That contact does not exist" << endl;
	}*/
	//cout << endl << endl << cm;

	return 0;
}