#include "ContactManager.h"

void ContactManager::add(Contact contact)
{
	name_map.add(contact.name, contact);
	phone_map.add(contact.phone, contact);
}

void ContactManager::remove(string name)
{
	try
	{
		phone_map.remove(name_map.get(name).phone); // This will throw KeyException if a contact
													// with that name does not exist
		name_map.remove(name);
	}
	catch (KeyException)
	{}
}

void ContactManager::update_phone(string name, string new_number)
{
	if (name_map.contains(name))
	{
		Contact contact = name_map.get(name);
		phone_map.remove(contact.phone);
		contact.phone = new_number;
		name_map.add(name, contact);
		phone_map.add(new_number, contact);
	}
	else
	{
		throw ContactMissingException();
	}
}

void ContactManager::update_email(string name, string new_email)
{
	if (name_map.contains(name))
	{
		Contact contact = name_map.get(name);
		contact.email = new_email;
		name_map.add(name, contact);
		phone_map.add(contact.phone, contact);
	}
	else
	{
		throw ContactMissingException();
	}
}

bool ContactManager::name_exists(string name)
{
	return name_map.contains(name);
}

bool ContactManager::phone_exists(string phone)
{
	return phone_map.contains(phone);
}

Contact ContactManager::get_contact_by_name(string name)
{
	try
	{
		return name_map.get(name);
	}
	catch (KeyException)
	{
		throw ContactMissingException();
	}
}

Contact ContactManager::get_contact_by_phone(string phone)
{
	try
	{
		return phone_map.get(phone);
	}
	catch (KeyException)
	{
		throw ContactMissingException();
	}
}

vector<Contact> ContactManager::get_contacts_by_name_prefix(string name_prefix)
{
	return name_map.prefix_search(name_prefix);
}

vector<Contact> ContactManager::get_contacts_by_phone_prefix(string phone_prefix)
{
	return phone_map.prefix_search(phone_prefix);
}

ostream& operator<< (ostream& out, ContactManager& manager)
{
	out << manager.name_map << endl << endl;
	out << manager.phone_map;
	return out;
}
