#include "AlienFamilyTree.h"

AlienFamilyTree::AlienFamilyTree()
{
    head = NULL;
}

AlienFamilyTree::~AlienFamilyTree()
{
    delete_tree(head);
    head = NULL;
}

bool AlienFamilyTree::is_descendant(string name)
{
    if (find(head, name) != NULL)
    {
        return 1;
    }

    return 0;
}

bool AlienFamilyTree::is_descendant(string ancestor, string descendant)
{
    NodePtr root = find(head, ancestor);

    if (root != NULL)
    {
        root = find(root, descendant);

        if (root != NULL)
        {
            return 1;
        }
    }

    return 0;
}

int AlienFamilyTree::generation(string name)
{
    return generation(head, name);
}

NodePtr AlienFamilyTree::find(NodePtr root, string name)
{
    if (root != NULL)
    {
        NodePtr found = NULL;
        if (root->data == name)
        {
            return root;
        }

        found = find(root->left, name);

        if (found != NULL)
        {
            return found;
        }

        found = find(root->right, name);

        if (found != NULL)
        {
            return found;
        }
    }

    return NULL;
}

int AlienFamilyTree::generation(NodePtr root, string name, int gen)
{
    if (root != NULL)
    {
        if (root->data == name)
        {
            return gen;
        }

        int found = generation(root->left, name, gen + 1);

        if (found != -1)
        {
            return found;
        }

        found = generation(root->right, name, gen + 1);

        if (found != -1)
        {
            return found;
        }
    }

    return -1;
}

void AlienFamilyTree::delete_tree(NodePtr root)
{
    if (root != NULL)
    {
        delete_tree(root->left);
        delete_tree(root->right);

        delete root;
    }
}

// Helper function that reads a binary tree in from the specified input stream
// and returns the root of the tree.
NodePtr parse_tree(istream& ins)
{
    char c;
    ins >> c; // Read (

    ins >> c; // Read next character

    // If the next character is ), this is the empty tree
    if(c == ')') {
        return NULL;
    }

    // Return the "borrowed" character to the input stream
    ins.putback(c);

    string node_data;
    ins >> node_data;

    NodePtr node = new BinaryStringNode;
    node->data = node_data;
    node->left = parse_tree(ins);
    node->right = parse_tree(ins);

    ins >> c; // Consume the trailing )

    return node;
}

void indent(ostream& outs, int level)
{
    for(int i = 0; i != level * 3; i++) {
        outs << " ";
    }
}

// Helper function that prints the binary tree root to the specified output
// stream.
void print_tree(ostream& outs, NodePtr root, int level = 0)
{
    indent(outs, level);
    if(root == NULL) {
        outs << "()" << endl;
        return;
    }
    outs << "(" << root->data << endl;
    print_tree(outs, root->left, level + 1);
    print_tree(outs, root->right, level + 1);
    indent(outs, level);
    outs << ")" << endl;
}

istream& operator>> (istream& ins, AlienFamilyTree& tree)
{
    tree.head = parse_tree(ins);
    return ins;
}

ostream& operator<< (ostream& outs, const AlienFamilyTree& tree)
{
    print_tree(outs, tree.head);
    return outs;
}

