#include <iostream>
#include "IntList.h"
#include "IntNode.h"
using namespace std;

IntList::IntList() {     // Constructor: creates an empty list
        head = NULL;
}

IntList::~IntList() {    // Destructor
        removeAll();
}

int IntList::length() { // Returns the length of the list
        int count = 0;
        for (NodePtr curr = head; curr != NULL; curr = curr->getLink(), count++);

        return count;
}

// Inserts a node with value the_number at the head of the list
void IntList::headInsert(int the_number) {
        if (emptyInsert(the_number))
        {
                return;
        }

        NodePtr tmp = head;             // Store the location of the first node
        head = new IntNode(the_number, tmp);             // Create new node
}

// Inserts a node with value the_number at the end of the list
void IntList::endInsert(int the_number) {
        if (emptyInsert(the_number))
        {
                return;
        }

        NodePtr curr;
        for (curr = head; curr->getLink() != NULL; curr = curr->getLink()); // Find the last node

        NodePtr tmp = curr;             // Store the location of the last node
        curr = new IntNode(the_number, NULL);             // Create new node
        tmp->setLink(curr);             // Set link of the last node to the new node
}

// Removes the first instance of a node with value the_number from the list
void IntList::remove(int the_number) {
        IntNode *prev = NULL,
                *curr = head,
                *next = curr->getLink();

        for (; curr != NULL; prev = curr, curr = curr->getLink(), next = curr->getLink())
        {
                if (curr->getData() == the_number) // If item is found
                {
                        delete curr;
                        if (prev == NULL) // If the item is first in the list
                        {
                                head = next;
                        }
                        else {
                                prev->setLink(next);
                        }
                        return;
                }
        } 
}

// Removes all the nodes in the list
void IntList::removeAll() {
        NodePtr tmp;
        for (NodePtr curr = head; curr != NULL; curr = tmp) // Itirates through the list
        {
                tmp = curr->getLink(); // Saving a pointer to the next element
                delete curr;
        }

        head = NULL; // The list is now empty
}

// Reverses the order of the nodes in the list
void IntList::reverse() {
        NodePtr prev = NULL;
        NodePtr curr = head;
        NodePtr next;

        while (curr != NULL)
        {
                next = curr->getLink();
                curr->setLink(prev);
                prev = curr;
                curr = next;
        }

        head = prev;
}

bool IntList::emptyInsert(int the_number) {
        if (head == NULL) // Special case for when the list is empty
        {
                head = new IntNode(the_number, NULL);
                return 1;
        }

        return 0;
}

ostream& operator <<(ostream& outs, const IntList& lis) {
        if (lis.head == NULL) // List is empty
        {
                outs << "List is empty";

                return outs;
        }

        for (NodePtr curr = lis.head; curr != NULL; curr = curr->getLink())
        {
                outs << curr->getData() << " ";
        }

        return outs;
}