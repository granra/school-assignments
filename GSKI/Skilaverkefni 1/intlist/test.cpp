#include <iostream>
#include "IntList.h"
using namespace std;

int main()
{
	IntList list;

	cout << list << endl;

	list.headInsert(1);
	list.endInsert(2);
	list.endInsert(3);
	list.endInsert(4);
	list.headInsert(0);
	cout << list.length() << endl;
	list.remove(0);
	cout << list << endl;
	list.reverse();

	cout << list << endl;

	cout << list.length() << endl;

	return 0;
}