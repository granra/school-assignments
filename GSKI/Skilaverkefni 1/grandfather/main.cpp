#include <iostream>
#include "StringSet.h"
using namespace std;

int main()
{
	string input;
	StringSet list;

	while (true)
	{
		cin >> input;
		if (input == "#")
		{
			break;
		}
		else if (input == "not")
		{
			cin >> input;
			list.remove(input);
		}
		else
		{
			list.insert(input);
		}
	}

	cout << list.size() << endl;
	cout << list;

	return 0;
}