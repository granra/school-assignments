 #include <iostream>
#include <string>
#include "StringNode.h"
#include "StringSet.h"
using namespace std;

// Constructor: creates an empty set.
StringSet::StringSet()
{
	head = NULL;
}

// Destructor.
StringSet::~StringSet()
{
	removeAll();
}

// Returns the number of elements in the set.
int StringSet::size()
{
	int count = 0;
    for (NodePtr curr = head; curr != NULL; curr = curr->getLink(), count++); // Counting

    return count;
}

// Inserts 'element' into the set. If 'element' is contained in the
// set, this operation has no effect.
void StringSet::insert(string element)
{
	if (head == NULL) // List is empty
	{
		head = new StringNode(element, NULL);
		return;
	}

	StringNode *prev = NULL, *curr = head;
	for (; curr != NULL; prev = curr, curr = curr->getLink()) // Check if element is already in there
	{
		if (curr->getData() == element)
		{
			return; // If the element is already in the list you do nothing and leave
		}
	}

	prev = NULL, curr = head;

	for (; curr != NULL; prev = curr, curr = curr->getLink())
	{
		if (element < curr->getData()) // If element is before current node in lexicographical order
		{
			NodePtr tmp = new StringNode(element, curr);
			if (prev == NULL)
			{
				head = tmp;
			}
			else {
				prev->setLink(tmp);
			}
			return;
		}
	}

	NodePtr tmp = new StringNode(element, curr);
	prev->setLink(tmp);
}

// Removes 'element' from the set. If 'element' is not in the set, this
// operation has no effect.
void StringSet::remove(string element)
{
	StringNode *prev = NULL, *curr = head;

	for (; curr != NULL; prev = curr, curr = curr->getLink()) // Iterating through the list
	{
		if (curr->getData() == element) // If element is found
		{
			if (prev == NULL) // If the element was found in the first node
			{
				head = curr->getLink();
			}
			else 
			{
				prev->setLink(curr->getLink());
			}
			
			delete curr;
			return; // Element was removed, no need to iterate through the rest
		}
	}
}

// Returns true if and only if 'element' is a member of the set.
bool StringSet::contains(string element)
{
	for (NodePtr curr = head; curr != NULL; curr = curr->getLink()) // Iterating through the list
	{
		if (curr->getData() == element) // If element was found
		{
			return true;
		}
	}

	return false;
}

// Removes all the nodes in the list
void StringSet::removeAll()
{
        NodePtr tmp;
        for (NodePtr curr = head; curr != NULL; curr = tmp) // Itirates through the list
        {
                tmp = curr->getLink(); // Saving a pointer to the next element
                delete curr;
        }

        head = NULL; // The list is now empty
}


// A friend function for writing the contents of the set to an output stream.
ostream& operator <<(ostream& outs, const StringSet& set)
{
        for (NodePtr curr = set.head; curr != NULL; curr = curr->getLink())
        {
                outs << curr->getData() << endl;
        }

        return outs;
}

bool operator <(const string lhs, const string rhs)
{
	if (lhs.compare(rhs) > 0)
	{
		return true;
	}
	return false;
}