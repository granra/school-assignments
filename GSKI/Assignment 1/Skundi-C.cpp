#include <iostream>
#include <vector>
using namespace std;

struct location {
	int x;
	int y;

	location() : x(0), y(0) {}
	location(int _x, int _y) {
		x = _x;
		y = _y;
	}
};

void movement();

void forward(char direction, location& current);

void turnLeft(char& direction);

void turnRight(char& direction);

vector<location> scan(vector<location> history);

int main()
{
	int n;
	cin >> n;

	for (int i = 0; i < n; ++i)
	{
		movement();
	}

	return 0;
}

void movement() {
	string operations;
	location current(0, 0); // This stores the location the robot is currently on.
	char direction = 'N';
	vector<location> history(1, location(0, 0)); // This vector stores each location the robot travels.

	cin >> operations;

	for (unsigned int i = 0; i < operations.length(); ++i)
	{
		switch(operations[i]){
				case 'F':
					forward(direction, current);
					history.push_back(location(current.x, current.y));
					break;
				case 'V':
					turnLeft(direction);
					break;
				case 'H':
					turnRight(direction);
					break;
		}
	}
	vector<location> trip = scan(history);

	for (int i = 0; i < (int)trip.size(); ++i)
	{
		cout << trip[i].x << " " << trip[i].y << endl;
	}
	cout << endl;

	history.clear();
}

void forward(char direction, location& current) {
	switch(direction) {
		case 'N':
			current.y++;
			break;
		case 'S':
			current.y--;
			break;
		case 'E':
			current.x++;
			break;
		case 'W':
			current.x--;
			break;
	}
}

void turnLeft(char& direction) {
	switch(direction){
		case 'N':
			direction = 'W';
			break;
		case 'W':
			direction = 'S';
			break;
		case 'S':
			direction = 'E';
			break;
		case 'E':
			direction = 'N';
			break;
	}
}

void turnRight(char& direction) {
	switch(direction){
		case 'N':
			direction = 'E';
			break;
		case 'E':
			direction = 'S';
			break;
		case 'S':
			direction = 'W';
			break;
		case 'W':
			direction = 'N';
			break;
	}
}

vector<location> scan(vector<location> history) {
	vector<location> trip;
	int loopEnd = 0;

	for (int i = 0; i < (int)history.size(); ++i)
	{
		for (int k = i + 1; k < (int)history.size(); ++k)
		{
			if (history[i].x == history[k].x && history[i].y == history[k].y)
			{
				loopEnd = k;
			}
		}
		trip.push_back(location(history[i].x, history[i].y));

		if (loopEnd != 0)
		{
			i = loopEnd;
			loopEnd = 0;
		}
	}

	return trip;
}