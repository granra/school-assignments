#include <iostream>
#include <cstring>
using namespace std;

struct location {
	int x;
	int y;

	location() : x(0), y(0) {}
	location(int _x, int _y) {
		x = _x;
		y = _y;
	}
};

void movement();

void forward(char direction, location& current);

void turnLeft(char& direction);

void turnRight(char& direction);

int main()
{
	int n;
	cin >> n;

	for (int i = 0; i < n; ++i)
	{
		movement();
	}

	return 0;
}

void movement() {
	string operations;
	location current;
	char direction = 'N';
	cin >> operations;

	for (unsigned int i = 0; i < operations.length(); ++i)
	{
		switch(operations[i]){
				case 'F':
					forward(direction, current);
					break;
				case 'V':
					turnLeft(direction);
					break;
				case 'H':
					turnRight(direction);
					break;
		}
	}	
	cout << current.x << " " << current.y << endl;
}

void forward(char direction, location& current) {
	switch(direction) {
		case 'N':
			current.y++;
			break;
		case 'S':
			current.y--;
			break;
		case 'E':
			current.x++;
			break;
		case 'W':
			current.x--;
			break;
	}
}

void turnLeft(char& direction) {
	switch(direction){
		case 'N':
			direction = 'W';
			break;
		case 'W':
			direction = 'S';
			break;
		case 'S':
			direction = 'E';
			break;
		case 'E':
			direction = 'N';
			break;
	}
}

void turnRight(char& direction) {
	switch(direction){
		case 'N':
			direction = 'E';
			break;
		case 'E':
			direction = 'S';
			break;
		case 'S':
			direction = 'W';
			break;
		case 'W':
			direction = 'N';
			break;
	}
}