#include <iostream>
#include "TravelMap.h"
using namespace std;

int main(int argc, char const *argv[])
{
	TravelMap map(10);
	map.add(0, 0);
	map.add(1, 0);
	map.add(2, 0);
	map.add(2, 1);
	map.add(1, 1);
	map.add(0, 1);
	//map.add(0, 0);
	map.add(0, -1);

	cout << map << endl;
	cout << map.is_circular() << endl;
	return 0;
}