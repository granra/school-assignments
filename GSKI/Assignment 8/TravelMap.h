#ifndef TRAVELMAP_H
#define TRAVELMAP_H

#include <iostream>
#include "CoordList.h"
using namespace std;

class TravelMap
{
private:
	CoordList** arr;
	int size;
	bool circular;

	unsigned int hash(const string s, int seed);

public:
	TravelMap(int size);
	~TravelMap();

	void add(int x, int y);
	friend ostream& operator << (ostream& out, const TravelMap& map);
	bool is_circular();
};

TravelMap::TravelMap(int size)
{
	this->size = size;
	this->arr = new CoordList*[size];
	this->circular = false;

	for (int i = 0; i < size; ++i)
	{
		arr[i] = NULL;
	}
}

TravelMap::~TravelMap()
{
	for (int i = 0; i < size; ++i)
	{
		delete arr[i];
	}
	delete[] arr;
}

void TravelMap::add(int x, int y)
{
	string keyString = (x + '0') + "-" + (y + '0');

	unsigned int key = hash(keyString, 12345) % size;

	if (arr[key] == NULL)
	{
		arr[key] = new CoordList();
	}

	if (arr[key]->contains(x, y))
	{
		circular = true;
	}
	else
	{
		arr[key]->add(x, y);
	}
}

bool TravelMap::is_circular()
{
	return circular;
}


// Murmur Hash af github síðu gagnaskipan 
unsigned int TravelMap::hash(const string s, int seed)
{
	int len = static_cast<int>(s.size());

	// 'm' and 'r' are mixing constants generated offline.
	// They're not really 'magic', they just happen to work well.
	const unsigned int m = 0x5bd1e995;
	const int r = 24;

	// Initialize the hash to a 'random' value
	unsigned int h = seed ^ len;

	// Mix 4 bytes at a time into the hash
	const unsigned char * data = (const unsigned char *)s.c_str();

	while(len >= 4) {
		unsigned int k = *(unsigned int *)data;

		k *= m;
		k ^= k >> r;
		k *= m;

		h *= m;
		h ^= k;

		data += 4;
		len -= 4;
	}

	// Handle the last few bytes of the input array

	switch(len) {
	        case 3:
	            h ^= data[2] << 16;
	        case 2:
	            h ^= data[1] << 8;
	        case 1:
	            h ^= data[0];
	            h *= m;
	};

	// Do a few final mixes of the hash to ensure the last few
	// bytes are well-incorporated.

	h ^= h >> 13;
	h *= m;
	h ^= h >> 15;

	return h;
}

ostream& operator << (ostream& out, const TravelMap& map)
{
	for (int i = 0; i < map.size; ++i)
	{
		out << i << ": ";
		if (map.arr[i] == NULL)
		{
			out << "Empty" << endl;
		}
		else
		{
			map.arr[i]->print_list();
		}
	}

	return out;
}

#endif