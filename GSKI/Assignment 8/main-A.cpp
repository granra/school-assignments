#include <iostream>
using namespace std;

void travel();

void forward(char direction, int& x, int& y);

void turnLeft(char& direction);

void turnRight(char& direction);

struct travelNode
{
	travelNode(int key, travelNode* left = NULL, travelNode* right = NULL);
	bool is_leaf();

	unsigned int key;
	travelNode* left;
	travelNode* right;
};

travelNode::travelNode(int key, travelNode* left, travelNode* right)
{
	this->key = key;
	this->left = NULL;
	this->right = NULL;
}

bool travelNode::is_leaf()
{
	return left == NULL && right == NULL;
}

class travelTree
{
private:
	travelNode* root;
	bool circular;
	void remove_all(travelNode*& node);
	void add(int key, travelNode*& node);
	unsigned int hash(const string s, int seed);
	void print_tree(travelNode* node, int level = 0);

public:
	travelTree();
	~travelTree();

	void add(int x, int y);
	bool is_circular();
	void print_tree();
};

int main()
{
	int n;
	cin >> n;

	for (int i = 0; i < n; ++i)
	{
		travel();
	}

	return 0;
}

void travel()
{
	travelTree tr;
	string operations;
	cin >> operations;
	int x = 0, y = 0;
	tr.add(x, y);
	char direction = 'N';

	for (unsigned int i = 0; i < operations.length(); ++i)
	{
		switch(operations[i]){
				case 'F':
					forward(direction, x, y);
					tr.add(x, y);
					break;
				case 'V':
					turnLeft(direction);
					break;
				case 'H':
					turnRight(direction);
					break;
		}
	}

	if (tr.is_circular())
	{
		cout << "Circular" << endl;
	}
	else
	{
		cout << "OK" << endl;
	}

	tr.print_tree();
}

void forward(char direction, int& x, int& y) {
	switch(direction) {
		case 'N':
			y++;
			break;
		case 'S':
			y--;
			break;
		case 'E':
			x++;
			break;
		case 'W':
			x--;
			break;
	}
}

void turnLeft(char& direction) {
	switch(direction){
		case 'N':
			direction = 'W';
			break;
		case 'W':
			direction = 'S';
			break;
		case 'S':
			direction = 'E';
			break;
		case 'E':
			direction = 'N';
			break;
	}
}

void turnRight(char& direction) {
	switch(direction){
		case 'N':
			direction = 'E';
			break;
		case 'E':
			direction = 'S';
			break;
		case 'S':
			direction = 'W';
			break;
		case 'W':
			direction = 'N';
			break;
	}
}

travelTree::travelTree()
{
	root = NULL;
	circular = false;
}

travelTree::~travelTree()
{
	remove_all(root);
}

void travelTree::remove_all(travelNode*& node)
{
	if (node != NULL)
	{
		remove_all(node->left);
		remove_all(node->right);
		delete node;
	}
}

void travelTree::add(int x, int y)
{
	string combined = "";
	combined += x + '0';
	combined += y + '0';

	unsigned int key = hash(combined, 15);

	add(key, root);
}

void travelTree::add(int key, travelNode*& node)
{
	if (node == NULL)
	{
		node = new travelNode(key);
		return;
	}
	if (key == node->key)
	{
		circular = true;
	}
	else if (key < node->key)
	{
		add(key, node->left);
	}
	else
	{
		add(key, node->right);
	}
}

bool travelTree::is_circular()
{
	return circular;
}

void travelTree::print_tree()
{
	print_tree(root);
}

void travelTree::print_tree(travelNode* node, int level)
{
	if (node != NULL)
	{
		for (int i = 0; i < level; ++i)
		{
			cout << "    ";
		}
		cout << node->key << endl;
		print_tree(node->left, level + 1);
		print_tree(node->right, level + 1);
	}
}

unsigned int travelTree::hash(const string s, int seed)
{
	int len = static_cast<int>(s.size());

	// 'm' and 'r' are mixing constants generated offline.
	// They're not really 'magic', they just happen to work well.
	const unsigned int m = 0x5bd1e995;
	const int r = 24;

	// Initialize the hash to a 'random' value
	unsigned int h = seed ^ len;

	// Mix 4 bytes at a time into the hash
	const unsigned char * data = (const unsigned char *)s.c_str();

	while(len >= 4) {
		unsigned int k = *(unsigned int *)data;

		k *= m;
		k ^= k >> r;
		k *= m;

		h *= m;
		h ^= k;

		data += 4;
		len -= 4;
	}

	// Handle the last few bytes of the input array

	switch(len) {
	        case 3:
	            h ^= data[2] << 16;
	        case 2:
	            h ^= data[1] << 8;
	        case 1:
	            h ^= data[0];
	            h *= m;
	};

	// Do a few final mixes of the hash to ensure the last few
	// bytes are well-incorporated.

	h ^= h >> 13;
	h *= m;
	h ^= h >> 15;

	return h;
}