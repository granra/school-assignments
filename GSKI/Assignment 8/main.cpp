#include <iostream>
#include "TravelMap.h"
using namespace std;

void travel();

void forward(char direction, int& x, int& y);

void turnLeft(char& direction);

void turnRight(char& direction);

int main()
{
	int n;
	cin >> n;

	for (int i = 0; i < n; ++i)
	{
		travel();
	}

	return 0;
}

void travel()
{
	TravelMap map(1000);
	string operations;
	cin >> operations;
	int x = 0, y = 0;
	map.add(x, y);
	char direction = 'N';

	for (unsigned int i = 0; i < operations.length(); ++i)
	{
		switch(operations[i]){
				case 'F':
					forward(direction, x, y);
					map.add(x, y);
					break;
				case 'V':
					turnLeft(direction);
					break;
				case 'H':
					turnRight(direction);
					break;
		}
	}

	if (map.is_circular())
	{
		cout << "Circular" << endl;
	}
	else
	{
		cout << "OK" << endl;
	}

	cout << map << endl;
}

void forward(char direction, int& x, int& y) {
	switch(direction) {
		case 'N':
			y++;
			break;
		case 'S':
			y--;
			break;
		case 'E':
			x++;
			break;
		case 'W':
			x--;
			break;
	}
}

void turnLeft(char& direction) {
	switch(direction){
		case 'N':
			direction = 'W';
			break;
		case 'W':
			direction = 'S';
			break;
		case 'S':
			direction = 'E';
			break;
		case 'E':
			direction = 'N';
			break;
	}
}

void turnRight(char& direction) {
	switch(direction){
		case 'N':
			direction = 'E';
			break;
		case 'E':
			direction = 'S';
			break;
		case 'S':
			direction = 'W';
			break;
		case 'W':
			direction = 'N';
			break;
	}
}