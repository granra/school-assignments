#ifndef COORDLIST_H
#define COORDLIST_H

#include <iostream>
#include "coordNode.h"
using namespace std;

class CoordList
{
private:
	coordNode* head;

	void remove_all(coordNode*& node);
	void print_list(coordNode* node);
	bool contains(int x, int y, coordNode* node);

public:
	CoordList();
	~CoordList();

	void add(int x, int y);
	void print_list();
	bool contains(int x, int y);
	bool empty();
};

CoordList::CoordList()
{
	head = NULL;
}

CoordList::~CoordList()
{
	remove_all(head);
}

void CoordList::remove_all(coordNode*& node)
{
	if (node != NULL)
	{
		remove_all(node->next);
		delete node;
	}
}

void CoordList::add(int x, int y)
{
	if (head == NULL)
	{
		head = new coordNode(x, y);
		return;
	}

	coordNode* temp = head;
	head = new coordNode(x, y, temp);
}

void CoordList::print_list()
{
	print_list(head);
	cout << endl;
}

void CoordList::print_list(coordNode* node)
{
	if (node != NULL)
	{
		cout << " -> " << node->x << ", " << node->y;
		print_list(node->next);
	}
}

bool CoordList::contains(int x, int y)
{
	return contains(x, y, head);
}

bool CoordList::contains(int x, int y, coordNode* node)
{
	if (node != NULL)
	{
		if (x == node->x && y == node->y)
		{
			return true;
		}

		return contains(x, y, node->next);
	}

	return false;
}

bool CoordList::empty()
{
	return head == NULL;
}

#endif