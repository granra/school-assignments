#ifndef COORDNODE_H
#define COORDNODE_H

struct coordNode
{
	coordNode(int x, int y, coordNode* next = NULL);

	int x, y;
	coordNode* next;
};

coordNode::coordNode(int x, int y, coordNode* next)
{
	this->x = x;
	this->y = y;
	this->next = next;
}

#endif