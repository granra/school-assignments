#include <iostream>
#include <string>
#include <assert.h>
#include "HuffmanNode.h"
#include "HuffmanPriorityQueue.h"

using namespace std;

const int INDENT_SIZE = 2;

// Prints the Huffman tree 'root' to the standard output, indented by 'indent' spaces.
// The printed tree is labeled (i.e., prefixed) by the string 'prefix'.
void print_tree(NodePtr root, int indent = 0, string prefix = "")
{
    // External nodes are not printed.
    if(root == NULL) {
        return;
    }

    char letter = ' ';
    if(root->is_leaf()) {
        letter = root->letter;
    }

    cout << string(indent, ' ') << prefix << "(" << letter << " [" << root->frequency << "]";
    if(root->is_leaf()) {
        cout << ")" << endl;
    } else {
        cout << endl;
        // Print left and right subtrees with the appropriate prefix, and
        // increased indent (by INDENT_SIZE).
        print_tree(root->left, indent + INDENT_SIZE, "0");
        print_tree(root->right, indent + INDENT_SIZE, "1");
        cout << string(indent, ' ') << ")" << endl;
    }
}

// Reads a frequency table from standard input, builds the Huffman tree for that frequency table and
// returns (a pointer to the root of) the tree.
NodePtr build_tree()
{
    HuffmanPriorityQueue queue; // Initialize queue
    int n;
    cin >> n; // Read in the number of nodes

    for (int i = 0; i < n; ++i)
    {
        char letter;
        int frequency;

        cin >> letter >> frequency; // Read in the letter and frequency of that letter

        queue.push(new HuffmanNode(frequency, letter)); // Push that to the queue
    }

    NodePtr root = NULL; // Initialize the root

    while (queue.size() > 1)
    {
        NodePtr left = queue.top();
        queue.pop();
        NodePtr right = queue.top();
        queue.pop();
        root = new HuffmanNode(left->frequency + right->frequency, ' '); // Make a new root
        root->left = left;      // Making a
        root->right = right;    // sub-tree

        queue.push(root); // Pushing the new sub-tree to the queue
    }

    return root;
}

// Returns the string obtained by decoding 'encoded_str' with the Huffman tree 'root' (or more
// specifically, the Huffman tree whose root is 'root').
string decode(NodePtr root, string encoded_str)
{
    string result;
    NodePtr sub = root;

    for (unsigned int i = 0; i <= encoded_str.size(); ++i) // Iterate through the string
    {
        if (sub->is_leaf())
        {
            result += string(1, sub->letter); // Append the letter to the result string
            sub = root; // Move back to the root
            --i; // Go back one letter since we didn't do anything with the last one
            continue;
        }

        switch (encoded_str[i])
        {
            case '0':
                sub = sub->left; // Move to the left sub-tree
                break;
            case '1':
                sub = sub->right; // Move to the right sub-tree
                break;
        }
    }

    return result;
}

// * Optional, but recommended *
// Returns true if and only if the tree 'root', contains the node with the letter 'letter'.
bool contains(NodePtr root, char letter)
{
    if (root != NULL) // Avoiding segmentation fault
    {
        if (root->letter == letter)
        {
            return true; // The letter is found
        }

        return (contains(root->left, letter) || contains(root->right, letter)); // Checking both sub-trees
    }

    return false;
}

// Returns the binary code obtained by encoding 'letter' with the Huffman tree 'root' (or more
// specifically, the Huffman tree whose root is 'root').
string encode(NodePtr root, char letter)
{
    string result;

    while (root->letter != letter)
    {
        if (contains(root->left, letter))
        {
            result += '0';
            root = root->left;
        }
        else if (contains(root->right, letter))
        {
            result += '1';
            root = root->right;
        }
    }

    return result;
}

// Frees (deallocates) the memory allocated for the Huffman tree 'root' (or more
// specifically, the Huffman tree whose root is 'root').
void free_memory(NodePtr root)
{
    if (root != NULL)
    {
        free_memory(root->left);
        free_memory(root->right);

        delete root;
    }
}

int main()
{
    // Read frequency table and build Huffman tree.
    NodePtr huffman = build_tree();
    print_tree(huffman);

    // Free the allocated memory.
    free_memory(huffman);

    return 0;
}

