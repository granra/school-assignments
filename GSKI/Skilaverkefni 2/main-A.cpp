#include <iostream>
#include <string>
#include <assert.h>
#include "HuffmanNode.h"
#include "HuffmanPriorityQueue.h"

using namespace std;

const int INDENT_SIZE = 2;

// Prints the Huffman tree 'root' to the standard output, indented by 'indent' spaces.
// The printed tree is labeled (i.e., prefixed) by the string 'prefix'.
void print_tree(NodePtr root, int indent = 0, string prefix = "")
{
    // External nodes are not printed.
    if(root == NULL) {
        return;
    }

    char letter = ' ';
    if(root->is_leaf()) {
        letter = root->letter;
    }

    cout << string(indent, ' ') << prefix << "(" << letter << " [" << root->frequency << "]";
    if(root->is_leaf()) {
        cout << ")" << endl;
    } else {
        cout << endl;
        // Print left and right subtrees with the appropriate prefix, and
        // increased indent (by INDENT_SIZE).
        print_tree(root->left, indent + INDENT_SIZE, "0");
        print_tree(root->right, indent + INDENT_SIZE, "1");
        cout << string(indent, ' ') << ")" << endl;
    }
}

// Reads a frequency table from standard input, builds the Huffman tree for that frequency table and
// returns (a pointer to the root of) the tree.
NodePtr build_tree()
{
    HuffmanPriorityQueue queue;
    int n;
    cin >> n;

    for (int i = 0; i < n; ++i)
    {
        char c;
        int count;

        cin >> c >> count;

        queue.push(new HuffmanNode(count, c));
    }

    NodePtr root = NULL;

    while (true)
    {
        NodePtr left = queue.top();
        queue.pop();
        NodePtr right = queue.top();
        queue.pop();
        root = new HuffmanNode(left->frequency + right->frequency, ' ');
        root->left = left;
        root->right = right;

        if (queue.empty())
        {
            break;
        }
        queue.push(root);
    }

    return root;
}

// Returns the string obtained by decoding 'encoded_str' with the Huffman tree 'root' (or more
// specifically, the Huffman tree whose root is 'root').
string decode(NodePtr root, string encoded_str)
{
    //TODO: Implement
    return "";
}

// * Optional, but recommended *
// Returns true if and only if the tree 'root', contains the node with the letter 'letter'.
bool contains(NodePtr root, char letter)
{
    //TODO: Implement
    return false;
}

// Returns the binary code obtained by encoding 'letter' with the Huffman tree 'root' (or more
// specifically, the Huffman tree whose root is 'root').
string encode(NodePtr root, char letter)
{
    //TODO: Implement
    return "";
}

// Frees (deallocates) the memory allocated for the Huffman tree 'root' (or more
// specifically, the Huffman tree whose root is 'root').
void free_memory(NodePtr root)
{
    if (root == NULL)
    {
        return;
    }

    free_memory(root->left);
    free_memory(root->right);

    delete root;
}

int main()
{
    // Read frequency table and build Huffman tree.
    NodePtr huffman = build_tree();
    print_tree(huffman);

    // Free the allocated memory.
    free_memory(huffman);

    return 0;
}

