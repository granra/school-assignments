#include "PrefixStringSet.h"
#include <iostream>
using namespace std;

PrefixStringSet::PrefixStringSet()
{
    root = new TrieNode();
}

PrefixStringSet::~PrefixStringSet()
{
    remove_all(root);
}

bool PrefixStringSet::insert(string s)
{
	bool is_substring = false;
	int size = s.length();
	NodePtr sub = root;

    for (int i = 0; i < size; ++i)
    {
    	int index = s[i] - '0';
    	bool last = i == size - 1;

    	if (sub->children[index] != NULL)
    	{
    		sub = sub->children[index];

    		if (sub->present == true)
    		{
    			is_substring = true;
    		}

    		if (last)
    		{
    			sub->present = true;
    			if (!sub->is_leaf())
    			{
    				is_substring = true;
    			}
    		}
    	}
    	else
    	{
    		if (last)
    		{
    			sub->children[index] = new TrieNode(true);
    		}
    		else
    		{
    			sub->children[index] = new TrieNode;
    			sub = sub->children[index];
    		}
    	}
    }

    return is_substring;
}

void PrefixStringSet::remove(string s)
{
    // OPTIONAL
}

bool PrefixStringSet::contains(string s)
{
	int size = s.length();
	NodePtr sub = root;

	for (int i = 0; i < size; ++i)
	{
		int index = s[i] - '0';

		if (sub->children[index] != NULL)
		{
			sub = sub->children[index];
		}
	}

	return sub->present;
}

void PrefixStringSet::remove_all(NodePtr node)
{
    if (node == NULL)
    {
    	return;
    }

    for (int i = 0; i < 10; ++i)
    {
    	remove_all(node->children[i]);
    }
    delete node;
}

