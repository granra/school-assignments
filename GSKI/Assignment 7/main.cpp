#include <iostream>
#include "PrefixStringSet.h"
using namespace std;

int main()
{
	PrefixStringSet set;
	int n;
	bool substring = false;
	
	cin >> n;

	for (int i = 0; i < n; ++i)
	{
		string input;
		cin >> input;

		if (substring == false && set.insert(input))
		{
			substring = true;
		}
	}

	if (!substring)
	{
		cout << "YES" << endl;
	}
	else
	{
		cout << "NO" << endl;
	}

	return 0;
}