#include "ContactList.h"

using namespace std;

ContactList::ContactList()
{
	head = NULL;
}

ContactList::~ContactList()
{
	remove_all();
}

void ContactList::remove_all()
{
	remove_all(head);

	head = NULL;
}

void ContactList::remove_all(NodePtr& node)
{
	if (node != NULL)
	{
		remove_all(node->link);
		delete node;
	}
}

bool ContactList::add(string key, Contact value)
{
	return add(head, key, value);
}

bool ContactList::add(NodePtr& node, string key, Contact value)
{
	if (node == NULL)
	{
		node = new ContactNode(key, value);
		return true;
	}
	else if (node->data.key == key)
	{
		node->data.value = value;
		return false;
	}

	return add(node->link, key, value);
}

bool ContactList::remove(string key)
{
	NodePtr prev = NULL;
	NodePtr node = head;

	for (; node != NULL; prev = node, node = node->link)
	{
		if (node->data.key == key)
		{
			if (node == head)
			{
				head = head->link;
				delete node;
			}
			else
			{
				prev->link = node->link;
				delete node;
			}

			return true;
		}
	}
	return false;
}

bool ContactList::contains(string key)
{
	for (NodePtr node = head; node != NULL; node = node->link)
	{
		if (node->data.key == key)
		{
			return true;
		}
	}
	return false;
}

Contact ContactList::get(string key)
{
	for (NodePtr node = head; node != NULL; node = node->link)
	{
		if (node->data.key == key)
		{
			return node->data.value;
		}
	}
	
	throw KeyException();
}

void ContactList::get_contacts(vector<Contact>& contacts)
{
	for (NodePtr node = head; node != NULL; node = node->link)
	{
		contacts.push_back(node->data.value);
	}
}

void ContactList::get_contacts_by_prefix(string prefix, vector<Contact>& contacts)
{
	for (NodePtr node = head; node != NULL; node = node->link)
	{
		if (lexico_string(prefix, node->data.key))
		{
			contacts.push_back(node->data.value);
		}
	}
}

bool ContactList::lexico_string(string prefix, string key)
{
	for (unsigned int i = 0; i < prefix.size(); ++i)
	{
		if (!(prefix[i] == key[i]))
		{
			return false;
		}
	}

	return true;
}

vector<StringContactPair> ContactList::to_vector()
{
	vector<StringContactPair> vec;
	for (NodePtr node = head; node != NULL; node = node->link)
	{
		vec.push_back(node->data);
	}
	return vec;
}

// Optionally implement.
ostream& operator <<(ostream& outs, const ContactList& lis)
{
	NodePtr node = lis.head;
	while (node != NULL)
	{
		outs << node->data.value << endl;
		node = node->link;
	}
	return outs;
}
