#include "StringContactMap.h"
#include "Hash.h"

#include <algorithm>

using namespace std;

StringContactMap::StringContactMap(int initial_capacity)
{
	this->map = new ListPtr[initial_capacity];
	this->capacity = initial_capacity;
	this->count = 0;

	initialize_array();
}

StringContactMap::~StringContactMap()
{
	for (int i = 0; i < capacity; ++i)
	{
		delete map[i];
	}

	delete[] map;
}

void StringContactMap::load_check()
{
	if (!((count / capacity) < MAX_LOAD))
	{
		rebuild();
	}
}

void StringContactMap::rebuild()
{
	ListPtr* tmpMap = map;
	int tmpCapacity = capacity;

	capacity *= 2;
	map = new ListPtr[capacity];
	count = 0;

	initialize_array();

	for (int i = 0; i < tmpCapacity; ++i)
	{
		if (tmpMap[i] != NULL)
		{
			vector<StringContactPair> vec = tmpMap[i]->to_vector();
			for (unsigned int j = 0; j < vec.size(); ++j)
			{
				add(vec[j].key, vec[j].value);
			}
		}
	}

	for (int i = 0; i < tmpCapacity; ++i)
	{
		delete tmpMap[i];
	}

	delete[] tmpMap;
}

void StringContactMap::initialize_array()
{
	for (int i = 0; i < capacity; ++i)
	{
		map[i] = NULL;
	}
}

int StringContactMap::size() const
{
	return count;
}

bool StringContactMap::empty() const
{
	return count == 0;
}

vector<Contact> StringContactMap::all_contacts() const
{
	vector<Contact> vec;
	for (int i = 0; i < capacity; ++i)
	{
		if (map[i] != NULL)
		{
			map[i]->get_contacts(vec);
		}
	}
	return vec;
}

void StringContactMap::add(string key, Contact value)
{
	load_check();

	int hashed = hash(key) % capacity;

	if (map[hashed] == NULL)
	{
		map[hashed] = new ContactList();
	}

	if (map[hashed]->add(key, value))
	{
		count++;
	}
}

void StringContactMap::remove(string key)
{
	int hashed = hash(key) % capacity;

	if (map[hashed] != NULL)
	{
		if (map[hashed]->remove(key))
		{
			count--;
		}
	}
}

bool StringContactMap::contains(string key) const
{
	int hashed = hash(key) % capacity;

	if (map[hashed] != NULL)
	{
		return map[hashed]->contains(key);
	}

	return false;
}

Contact StringContactMap::get(string key) const
{
	int hashed = hash(key) % capacity;

	if (map[hashed] != NULL)
	{
		return map[hashed]->get(key);
	}

	throw KeyException();
}

vector<Contact> StringContactMap::prefix_search(string prefix) const
{
	vector<Contact> vec;
	for (int i = 0; i < capacity; ++i)
	{
		if (map[i] != NULL)
		{
			map[i]->get_contacts_by_prefix(prefix, vec);
		}
	}
	return vec;
}

// Optionally implement.
ostream& operator <<(ostream& out, const StringContactMap& contact_map)
{
	for (int i = 0; i < contact_map.capacity; ++i)
	{
		if (contact_map.map[i] != NULL)
		{
			out << *contact_map.map[i];
		}
	}

	return out;
}
