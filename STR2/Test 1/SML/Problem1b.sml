fun		prefix(nil, M)		= true
  |		prefix(N, nil)		= false
  |		prefix(n::N, m::M)	= if n=m then true andalso prefix(N, M)
  							  else 		  false;