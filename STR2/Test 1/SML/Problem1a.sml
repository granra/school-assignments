fun 	length(nil) 	= 0
  | 	length(n::L) 	= 1 + length(L);